import time
import random
amount_of_iteration = 10000000 # amount of iterations for each operation
list_for_add_sbt = [3,5,7,11,13,17,19,23,29,31]

def avarage_time_of_empty_cycle():
    def f():
        time1 = time.time()
        for i in range(10000000):
            b = list_for_add_sbt[random.randint(0,9)]
            c = list_for_add_sbt[random.randint(0,9)]
        time2 = time.time()
        time_t = time2-time1
        return time_t

    time_of_empty_it = []
    for y in range(1):
        t = f()
        time_of_empty_it.append(t)

    avarage = sum(time_of_empty_it)/len(time_of_empty_it)
    return avarage

avarage = 0

# print(avarage_time_of_empty_cycle())
def additional_int(amount_of_iteration):
    time_start = time.time()
    for a in range(amount_of_iteration):
        b = list_for_add_sbt[random.randint(0,9)]
        c = list_for_add_sbt[random.randint(0,9)]
        a = b + c
    time_end = time.time()
    total_time = (time_end - time_start) - avarage
    amount_per_sec = 1/(total_time/amount_of_iteration)
    return total_time, amount_per_sec
    
def additional_float(amount_of_iteration):
    time_start = time.time()
    for a in range(amount_of_iteration):
        b = random.random()
        c = random.random()
        a = b + c 
    time_end = time.time()
    total_time = (time_end - time_start) - avarage
    amount_per_sec = 1/(total_time/amount_of_iteration)
    return total_time, amount_per_sec
    
def subtraction_int(amount_of_iteration):
    time_start = time.time()
    for a in range(amount_of_iteration):
        b = list_for_add_sbt[random.randint(0,9)]
        c = list_for_add_sbt[random.randint(0,9)]
        a = b - c
    time_end = time.time()
    total_time = (time_end - time_start) - avarage
    amount_per_sec = 1/(total_time/amount_of_iteration)
    return total_time, amount_per_sec

def subtraction_float(amount_of_iteration):
    time_start = time.time()
    for a in range(amount_of_iteration):
        b = random.random()
        c = random.random()
        a = b - c
    time_end = time.time()
    total_time = (time_end - time_start) - avarage
    amount_per_sec = 1/(total_time/amount_of_iteration)
    return total_time, amount_per_sec

def multiplication_int(amount_of_iteration):
    time_start = time.time()
    for a in range(amount_of_iteration):
        b = list_for_add_sbt[random.randint(0,9)]
        c = list_for_add_sbt[random.randint(0,9)]
        a = b * c
    time_end = time.time()
    total_time = (time_end - time_start) - avarage
    amount_per_sec = 1/(total_time/amount_of_iteration)
    return total_time, amount_per_sec

def multiplication_float(amount_of_iteration):
    time_start = time.time()
    for a in range(amount_of_iteration):
        b = random.random()
        c = random.random()
        a = b * c
    time_end = time.time()
    total_time = (time_end - time_start) - avarage
    amount_per_sec = 1/(total_time/amount_of_iteration)
    return total_time, amount_per_sec

def division_int(amount_of_iteration):
    time_start = time.time()
    for a in range(amount_of_iteration):
        b = list_for_add_sbt[random.randint(0,4)]
        c = list_for_add_sbt[random.randint(5,9)]
        a = c/b
    time_end = time.time()
    total_time = (time_end - time_start) - avarage
    amount_per_sec = 1/(total_time/amount_of_iteration)
    return total_time, amount_per_sec

def division_float(amount_of_iteration):
    time_start = time.time()
    for a in range(amount_of_iteration):
        b = random.random() + 1
        c = random.random()
        a = b/c
    time_end = time.time()
    total_time = (time_end - time_start) - avarage
    amount_per_sec = 1/(total_time/amount_of_iteration)
    return total_time, amount_per_sec

def result(amount_of_iteration):
    """ function to print the result of testing """
    operation_time = [] # the list to collect total time of each operation 
    total_time_add_int, amount_per_sec_add_int = additional_int(amount_of_iteration)
    operation_time.append(total_time_add_int)

    total_time_add_float, amount_per_sec_add_float = additional_float(amount_of_iteration)
    operation_time.append(total_time_add_float)

    total_time_sbt_int, amount_per_sec_sbt_int = subtraction_int(amount_of_iteration)
    operation_time.append(total_time_sbt_int)

    total_time_sbt_float, amount_per_sec_sbt_float = subtraction_float(amount_of_iteration)
    operation_time.append(total_time_sbt_float)

    total_time_mlt_int, amount_per_sec_mlt_int = multiplication_int(amount_of_iteration)
    operation_time.append(total_time_mlt_int)

    total_time_mlt_float, amount_per_sec_mlt_float = multiplication_float(amount_of_iteration)
    operation_time.append(total_time_mlt_float)

    total_time_dvs_int, amount_per_sec_dvs_int = division_int(amount_of_iteration)
    operation_time.append(total_time_dvs_int)

    total_time_dvs_float, amount_per_sec_dvs_float = division_float(amount_of_iteration)
    operation_time.append(total_time_dvs_float)

    min = 1000000 # just a random big amount to define the fastest operation
    min_i = None # index of operation to defind the fastest operation
    for i in range(len(operation_time)):
        if operation_time[i] < min:
            min = operation_time[i]
            min_i = i

    
    def print_add_int(amount_per_sec_add_int, percent):
        bar = '#'*int(round((50*percent),0)) + ' '*(50 - int(round((50*percent),0)))
        print(f'+\tint\t{amount_per_sec_add_int:.6}\t{bar}\t{int(round((percent*100),0))}%\n')

    def print_add_float(amount_per_sec_add_float, percent):
        bar = '#'*int(round((50*percent),0)) + ' '*(50 - int(round((50*percent),0)))
        print(f'+\tfloat\t{amount_per_sec_add_float:.6}\t{bar}\t{int(round((percent*100),0))}%\n')

    def print_sbt_int(amount_per_sec_sbt_int, percent):
        bar = '#'*int(round((50*percent),0)) + ' '*(50 - int(round((50*percent),0)))
        print(f'-\tint\t{amount_per_sec_sbt_int:.6}\t{bar}\t{int(round((percent*100),0))}%\n')

    def print_sbt_float(amount_per_sec_sbt_float, percent):
        bar = '#'*int(round((50*percent),0)) + ' '*(50 - int(round((50*percent),0)))
        print(f'-\tfloat\t{amount_per_sec_sbt_float:.6}\t{bar}\t{int(round((percent*100),0))}%\n')
    
    def print_mlt_int(amount_per_sec_mlt_int, percent):
        bar = '#'*int(round((50*percent),0)) + ' '*(50 - int(round((50*percent),0)))
        print(f'*\tint\t{amount_per_sec_mlt_int:.6}\t{bar}\t{int(round((percent*100),0))}%\n')
    
    def print_mlt_float(amount_per_sec_mlt_float, percent):
        bar = '#'*int(round((50*percent),0)) + ' '*(50 - int(round((50*percent),0)))
        print(f'*\tfloat\t{amount_per_sec_mlt_float:.6}\t{bar}\t{int(round((percent*100),0))}%\n')
    
    def print_dvs_int(amount_per_sec_dvs_int, percent):
        bar = '#'*int(round((50*percent),0)) + ' '*(50 - int(round((50*percent),0)))
        print(f'/\tint\t{amount_per_sec_dvs_int:.6}\t{bar}\t{int(round((percent*100),0))}%\n')
    
    def print_dvs_float(amount_per_sec_dvs_float, percent):
        bar = '#'*int(round((50*percent),0)) + ' '*(50 - int(round((50*percent),0)))
        print(f'/\tfloat\t{amount_per_sec_dvs_float:.6}\t{bar}\t{int(round((percent*100),0))}%\n')

    print_add_int(amount_per_sec_add_int,(operation_time[min_i]/operation_time[0]))
    print_sbt_int(amount_per_sec_sbt_int,(operation_time[min_i]/operation_time[2]))
    print_mlt_int(amount_per_sec_mlt_int,(operation_time[min_i]/operation_time[4]))
    print_dvs_int(amount_per_sec_dvs_int,(operation_time[min_i]/operation_time[6]))
    print_add_float(amount_per_sec_add_float,(operation_time[min_i]/operation_time[1]))
    print_sbt_float(amount_per_sec_sbt_float,(operation_time[min_i]/operation_time[3]))
    print_mlt_float(amount_per_sec_mlt_float,(operation_time[min_i]/operation_time[5]))
    print_dvs_float(amount_per_sec_dvs_float,(operation_time[min_i]/operation_time[7]))  

result(amount_of_iteration)
